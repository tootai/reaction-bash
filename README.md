# reaction.sh

Script bash compatible avec le projet https://reaction.ppom.me un logiciel équivalent à fail2ban

version 0.2.3
=============
- BUG en utilisation avec iptables, les règles n'étaient pas appliquées en raison du test de la table d'affectation
- on peut forcer l'utilisation d'iptables en passant le paramètre usenft à false (iptables-nft)
 
version 0.2.2
=============
FR
- les outils pour manipuler les réseaux/ip sont dorénavant dans iptools.inc.sh appelé à partir de reaction.inc.sh
EN
- tools to manipulate nets/ips are now in iptools.inc.sh called from reaction.inc.sh

Fonctions du script dans sa version 0.2.1 - 20240427
===================
- remplace les programmes ip46tables et nft46
- à partir de la définition des règles de ban nftables, les actions sont transcrites dans iptables si nftables est absent;
  ceci permet une configuration unique de reaction.jsonnet basée sur nftables et ce peu importe le pare-feu utilisé
- ban manuel d'une IP via reaction
- unban manuel d'une IP via reaction
- en cas de ban un envoi de courriel avec possibilité de rajouter la location geoip est possible (gestion par stream)
- en cas de ban une alerte telegram est possible (script telegram.sh non inclu) (gestion par stream)
- en cas de ban une alerte signal est possible (script api4signal.sh non inclu) (gestion par stream)
- la commande services permet de lancer ce script à partir de ip46tables ou nft46 en passant la commande en paramètre

Note le fichier reaction.inc.sh contient les paramètres utilisateur
----

Script functions in version 0.2.1 - 20240427
================
- replacement for programs ip46tables et nft46
- from definition of ban rules compatible nftables, actions can be applied to iptables if nftables is not installed;
- this allow a unique configuration of reaction.jsonnet file based on nftables independant of installed firewall
- manual IP ban via reaction
- manual IP unban via reaction
- possibility in case of ban to send email including IP geo location through geoip (per stream)
- possibility in case of ban to send alert using telegram (script telegram.sh not included) (per stream)
- possibility in case of ban to send alert using signal (script ap4signal.sh not included) (per stream)
- service command permit to launch this script from ip46tables or nft46 passing command to execute as parameter

Note reaction.inc.sh file contain user parameters
----
