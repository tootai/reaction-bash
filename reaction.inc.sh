# *** User datas ***
#
# . table, setpfx and sets name must follow those given in reaction.jsonnet file (or vice versa)
# . datas in myports and myprotos arrays must follow the set row they belong
#

mysets=("" "smtp" "web" "asterisk" "ssh")
myports=("all" "25,465,587" "80,443,8080" "4569,5060,5061" "22")
myprotos=("all" "tcp" "tcp" "tcp,udp" "tcp")
mysetsAlert=("all")                                     # for which stream(s) we want to receive notification when an IP is (un)banned
                                                        # set to ALL for all streams
mytable="reaction"                                      # table name to use for nftables. iptables will use filter table.
mysetpfx="ban"                                          # set prefix to use
myfile2reaction="/var/log/reaction/toban"               # path to file setted in jsonnet for manual ban

sendMail=no                                             # should we get email for notification
sendUnbanMail=no                                        # should we get email notification when a rule is deleted
sendTelegram=no                                         # should we get Telegram message for notification
sendSignal=no                                           # should we get Signal message for notification (not yet implemented)
withWhois=no                                            # should we add whois information into mail message
mailadmin="tech@tootai.net"                             # who will get emails notification

verbose=0                                               # =0 quiet =1 system logs

myiptools="/usr/local/bin/iptools.inc.sh"
! [ -r "$myiptools" ] && logger "reaction - Can't locate <$myptools> - EXIT" && exit 0
. $myiptools

#
# End of user datas
