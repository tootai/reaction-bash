#!/bin/bash
##############################################################################
#
# reaction.sh create and delete table, sets and rules based on services, protocols or ports
#
# Copyright (C) 2024 (Daniel Huhardeaux devel@tootai.net)
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence Publique Générale GNU
# publiée par la Free Software Foundation (version 2 ou bien toute autre version ultérieure choisie par vous).
#
# Ce programme est distribué car potentiellement utile, mais SANS AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties
# de commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU en même temps que ce programme;
# si ce n'est pas le cas, écrivez à la Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, États-Unis.
# ou visitez <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#############################################################################

#############################################################################
#
#   Install this script in /usr/local/bin or any other directory in your PATH
#
#   Modifications:
#
#   20240326    add log stuff
#
#   20240405    add whois stuff
#               add print version
#
#   20240418    add iptables/ip6tables
#   v0.2.0      All commands are extracted from nftables one
#               which we treat as our default firewall program
#               *** No need to modify reaction.jsonnet file ***
#
#   20240427    add possibility to ban/unban IPs manually by advertising
#   v0.2.1      reaction.Those IPs are banned inside the empty set (all ports)
#
#   20240503    validation of nets/ips are now calculate from iptools.inc.sh
#   v0.2.2      include, where path is given inside reaction.inc.sh
#
#   20240706    . test of existing table in SetIPTypeAndExecute is only done
#   v0.2.3      if nft is used
#               . add possibility to force use of ip[6]tables by setting usenft
#               to false
#
#############################################################################

# Output version
#
if [ "$1" = "-v" -o "$1" = "--version" ]; then
  echo && echo "$(basename $0) - version 0.2.2" && echo
  exit 0
fi

# Binaries check, nftables preferred to iptables
#
fwtables=$(whereis nft|grep bin|cut -d \  -f2)
fw6tables=
usenft=true

if [ "x$fwtables" = "x" -o "$usenft" = "false" ]; then
  # nftables is missing, check iptables & ip6tables
  #
  fwtables=$(whereis iptables|grep bin|cut -d \  -f2)
  fw6tables=$(whereis ip6tables|grep bin|cut -d \  -f2)
  [ "x$fwtables" = "x" -o "x$fw6tables" = "x" ] && "reaction - Missing nftables, iptables or ip6tables, EXIT" && exit 1

  fwversion=$($fwtables --version|cut -d " " -f 2|sed 's/v//'|sed 's/\.//g')
  usenft=false
else
  fwversion=$($fwtables -v|cut -d " " -f 2|sed 's/v//'|sed 's/\.//g')
fi

logger "reaction - Start procedure for command $1 (option=$2)"

# If nftables binary is not found or sended commands are unknown, exit with failure
#
# cmd=ban/unban => action sended by reaction
# cmd=add/del => action manually sended
#
if [ "$1" != "start" -a "$1" != "stop" -a "$1" != "services" -a "$1" != "ban" -a "$1" != "unban" -a "$1" != "add" -a "$1" != "del" -o "x$fwtables" = "x" ]; then
  exit 1
fi

mycmd=$1                                                # cmd line
myservices=$2                                           # cmd line options

# *** User datas ***
#

user_datas="/usr/local/bin/reaction.inc.sh"             # where lies user datas
! [ -r "$user_datas" ] && logger "reaction - User datas file ($user_datas) not found, EXIT" && exit 1
. $user_datas

#
# *** END ***

reaction=$(whereis reaction|grep bin|cut -d \  -f2)     # reaction binarie
mailcmd=$(whereis mail|grep bin|cut -d \  -f2)          # bsd-mailx is a good choice
telegram=$(whereis telegram.sh|grep bin|cut -d \  -f2)  # telegram.sh is a bash script using telegram API
signalcli=                                              # api4signal.sh is a bash script using signal API
mywhois=$(whereis whois|grep bin|cut -d \  -f2)         # whois program if we want additional datas
myhostname=$(hostname|cut -d . -f1)
tmpvar=
tmpset=
OLDIFS=$IFS

#
# End of various datas

AdviseReaction() {
  [ "x$reaction" = "x" ] && logger "reaction - binarie in PATH, no action taken" && exit 0     # reaction not found in PATH, exit

  # check if IPv6 address
  #
  tmpvar=$(valid_net6 $myservices)
  if [ $? -eq 1 ]; then
    # check if IPv6 address
    #
    tmpvar=$myservices
    tmpvar=$(valid_net4 $myservices)
    [ $? -eq 1 ] && exit 0                                  # not a valid IP address, exit silently
  fi

  [ $verbose -eq 1 ] && logger "reaction - Advise <reaction> to $mycmd IP $tmpvar"

  if [ "$mycmd" = "add" ]; then
    echo $tmpvar>$myfile2reaction                           # IP address in file connected to reaction 
  else
    $reaction flush $tmpvar>/dev/null                       # run reaction flush action
  fi
}

SetIPTypeAndExecute() {
  [ $verbose -eq 1 ] && logger "reaction - Extract IP and set rule for '$myservices'"
  [ "$usenft" = "false" ] && mytable="filter"   # default iptables table

  # If table name doesn't correspond to script defined one, exit silently
  #
  [ "$usenft" = "true" -a "$(echo $myservices|cut -d '{' -f1|cut -d ' ' -f4|sed 's/\ //g')" != "$mytable" ] && exit 0

  # If set name doesn't include the X character for ip address type of 4 or 6, exit silently
  #
  tmpset=$(echo "$myservices"|cut -d '{' -f1|cut -d ' ' -f5|sed 's/\ //g')
  [ "z$(echo $tmpset|grep X)" = "z" ] && exit 0

  # Extract the IP address
  #
  tmpvar="$(echo $myservices|cut -d '{' -f2|cut -d '}' -f1|sed 's/\ //g')"
  [ $verbose -eq 1 ] && logger "reaction - IP address is '$tmpvar'"

  # check if IPv6 address
  #
  tmpvar1=$(valid_ip6 $tmpvar)
  if [ $? -eq 0 ]; then
    [ $verbose -eq 1 ] && logger "reaction - IP address is of type IPv6"
    myservices=$(echo $myservices|sed 's/X/6/')
    myiptype=6
  else
    # check if IPv4 address
    #
    tmpvar1=$(valid_ip4 $tmpvar)
    if [ $? -eq 0 ]; then
      [ $verbose -eq 1 ] && logger "reaction - IP address is of type IPv4"
      myservices=$(echo $myservices|sed 's/X/4/')
      myiptype=4
    else
      [ $verbose -eq 1 ] && logger "reaction - IP address is of type UNKNOWN ($tmpvar)"
      myservices=
      myiptype=
    fi
  fi

  # if IP is valid we execute command ban or unban and run services for notifications only if stream is in list
  #
  if [ "x$myservices" != "x" ]; then
    if [ "$usenft" = "true" ]; then
      fwexec="$fwtables $myservices"
    else
      # For iptables ban/unban is the first command passed by reaction
      # 
      tmpset=$(echo "$myservices"|cut -d '{' -f1|cut -d ' ' -f5|sed 's/\ //g')  # extract final chain name
      [ "$mycmd" = "ban" ] && sens="-A" || sens="-D"                            # add or delete rule

      # Checking if the rule is existing
      # if (ban and exist) or (unban and not existing) => exit
      #
      myservices="-C $tmpset -s $tmpvar -j DROP"
      [ "x$myiptype" = "x4" ] && fwexec="$fwtables $myservices" || fwexec="$fw6tables $myservices"
      $fwexec 2>/dev/null
      status=$?
      ([ $status -eq 0 ] && [ "$sens" = "-A" ]) || ([ $status -eq 1 ] && [ "$sens" = "-D" ]) && exit 0

      # Do action
      #
      myservices="$sens $tmpset -s $tmpvar -j DROP"
      [ "x$myiptype" = "x4" ] && fwexec="$fwtables $myservices" || fwexec="$fw6tables $myservices"
    fi

    $fwexec                                                                     # execute cmd, nftables or ip|6|tables
    [ $verbose -eq 1 ] && logger "reaction - Set rule '$fwexec'" 

    for i in $(seq 0 $((${#mysetsAlert[@]}-1))); do
      tmpvar=$(echo $myservices|grep ${mysetsAlert[$i]})

      if [ "x$tmpvar" != "x" -o "${mysetsAlert[0]}" = "all" -o "${mysetsAlert[0]}" = "ALL" ]; then
        services "$(echo $myservices|cut -d " " -f4,5,6,7|sed 's/{//g')"
      fi
    done
  fi
}

addRulesProto() {
  if [ "$usenft" = "true"]; then
    $fwtables add rule inet $mytable input ip $myruleproto ip saddr @$mysetpfx"4"$tmpsfx counter drop
    $fwtables add rule inet $mytable input ip6 $myruleproto ip6 saddr @$mysetpfx"6"$tmpsfx counter drop 
  else
    $fwtables  -I INPUT 1 -p $myruleproto -j $mysetpfx"4"$tmpsfx
    $fw6tables -I INPUT 1 -p $myruleproto -j $mysetpfx"6"$tmpsfx
  fi
}

addRulesPorts() {
  if [ "$usenft" = "true" ]; then
    $fwtables add rule inet $mytable input ip saddr @$mysetpfx"4"$tmpsfx $myruleproto dport { ${myports[$i]} } counter drop
    $fwtables add rule inet $mytable input ip6 saddr @$mysetpfx"6"$tmpsfx $myruleproto dport { ${myports[$i]} } counter drop 
  else
    $fwtables  -I INPUT 1 -p $myruleproto --match multiport --dport ${myports[$i]} -j $mysetpfx"4"$tmpsfx
    $fw6tables -I INPUT 1 -p $myruleproto --match multiport --dport ${myports[$i]} -j $mysetpfx"6"$tmpsfx
  fi
}

rules() {
  case $1 in
    proto)
      # Set rules for protocols
      #
      myruleproto=""

      if [ "x$(echo ${myprotos[$i]}|grep -i tcp)" != "x" ]; then
        myruleproto="protocol tcp"
      fi

      if [ "x$(echo ${myprotos[$i]}|grep -i udp)" != "x" ]; then
        if [ "$myruleproto" = "tcp" ]; then
          addRulesProto
        fi
        myruleproto="protocol udp"
      fi

      if [ "x$myruleproto" = "x" ]; then
        if [ "$usenft" = "true" ]; then
          # Those rules will block all traffic beloging to ip/ip6 source address
          #
          $fwtables add rule inet $mytable input ip saddr @$mysetpfx"4"$tmpsfx counter drop
          $fwtables add rule inet $mytable input ip6 saddr @$mysetpfx"6"$tmpsfx counter drop 
        else
          $fwtables  -I INPUT 1 -j $mysetpfx"4"$tmpsfx
          $fw6tables -I INPUT 1 -j $mysetpfx"6"$tmpsfx
        fi
      else
        addRulesProto
      fi
      ;;
    *)
      # Set rules for ports & prototols
      #
      myruleproto=""

      if [ "x$(echo ${myprotos[$i]}|grep -i tcp)" != "x" ]; then
        myruleproto="tcp"
      fi

      if [ "x$(echo ${myprotos[$i]}|grep -i udp)" != "x" ]; then
        if [ "$myruleproto" = "tcp" ]; then
          addRulesPorts
        fi
        myruleproto="udp"
      fi

      if [ "x$myruleproto" = "x" ]; then
        myruleproto="tcp"
        addRulesPorts
        myruleproto="udp"
        addRulesPorts
      else
        addRulesPorts
      fi    
      ;;
  esac
}

Table() {
  # Create table
  #
  if [ "$usenft" = "true" ]; then
    [ "$1" = "create" ] && $fwtables add table inet $mytable || $fwtables delete table inet $mytable 
  fi
}

SetsAndRules() {
  myaction=$1

  for i in $(seq 0 $((${#mysets[@]}-1))); do
    [ "x${mysets[$i]}" = "x" ] && tmpsfx="" || tmpsfx="-${mysets[$i]}"

    if [ "$usenft" = "true" ]; then
      if [ "$myaction" = "create" ]; then
        # Create sets and chain
        # auto-merge functional starting from nftables v1.0.7
        # 
        [ $fwversion -ge 107 ] && automerge="auto-merge;" || automerge=

        $fwtables add set inet $mytable $mysetpfx"4"$tmpsfx { type ipv4_addr\; flags interval\; $automerge }
        $fwtables add set inet $mytable $mysetpfx"6"$tmpsfx { type ipv6_addr\; flags interval\; $automerge }
        $fwtables add chain inet $mytable input { type filter hook input priority -100\; policy accept\; }
      else
        # Flush sets
        #
        $fwtables flush set inet $mytable $mysetpfx"4"$tmpsfx
        $fwtables flush set inet $mytable $mysetpfx"6"$tmpsfx
      fi
    else
      if [ "$myaction" = "create" ]; then
        # Create chain
        #
        $fwtables  -N $mysetpfx"4"$tmpsfx
        $fwtables  -F $mysetpfx"4"$tmpsfx
        $fw6tables -N $mysetpfx"6"$tmpsfx
        $fw6tables -F $mysetpfx"6"$tmpsfx
      else
        # Delete INPUT rule, flush and delete chain, ipv4 and ipv6
        # lines MUST be deleted in reversal order !!!
        mylines=$(echo $($fwtables -L INPUT --line-number|grep $mysetpfx"4"$tmpsfx" "|cut -d " " -f1)|rev)
        for i in $mylines; do $fwtables -D INPUT $i; done 
        $fwtables  -F $mysetpfx"4"$tmpsfx
        $fwtables  -X $mysetpfx"4"$tmpsfx

        mylines=$(echo $($fw6tables -L INPUT --line-number|grep $mysetpfx"6"$tmpsfx" "|cut -d " " -f1)|rev)
        for i in $mylines; do $fw6tables -D INPUT $i; done
        $fw6tables -F $mysetpfx"6"$tmpsfx
        $fw6tables -X $mysetpfx"6"$tmpsfx
      fi
    fi

    # In case of creation, table, chain and sets are setted, now the rules
    #
    if [ "$myaction" = "create" ]; then  
      # Create rules
      #
      case "${myports[$i]}" in
        "" | "all" | "ALL")
          rules proto
          ;;
        *)
          rules ports
          ;;
      esac
    fi
  done
}

sendMail() {
  [ "$sendMail" != "yes" -o "x$mailcmd" = "x" -o "x$mailadmin" = "x" -o "x$1" = "x" ] && exit 0
  [ $verbose -eq 1 ] && logger "reaction - Send EMAIL for IP $1"

  # If we don't want mail on unban action (mycmd=unban or myservices contain delete), exit
  #
  if [ "$sendUnbanMail" = "no" ]; then
    [ "$mycmd" = "unban" -o "x$(echo '$myservices'|grep 'delete')" != "x" ] && exit 0
  fi

  # If whois details should be added to message
  #
  if [ "$withWhois" = "yes" -a "x$mywhois" != "x" ]; then
    tmpvar=$(echo $1|cut -d " " -f3)  # Extract IP address in case of whois
    message=$($mywhois $tmpvar)

    [ $verbose -eq 1 ] && logger "reaction - withWhois=$withWhois mywhois=$mywhois message=$message"
    echo "$message"|sed '/^#/d'|sed '/^%/d'|sed '/^$/N;/^\n$/D'|mail -s "$myhostname - $1" $mailadmin
  else
    # No whois, empty message
    #
    mail -s "$myhostname - $1" $mailadmin </dev/null #2>&1>/dev/null
  fi
}

sendTelegram() {
  [ "$sendTelegram" != "yes" -o "x$telegram" = "x" ] && exit 0
  [ $verbose -eq 1 ] && logger "reaction - Send TELEGRAM message for IP $1"
  $telegram "msg $myhostname - $1"
}

sendSignal() {
  [ "$sendSignal" != "yes" ] && exit 0 
  [ $verbose -eq 1 ] && logger "reaction - Send SIGNAL message for IP $1"
}

services() {
  sendMail "$1"
  sendTelegram "$1"
  sendSignal "$1"
}

# Main program
#

case "$mycmd" in
  "start")
    Table create
    SetsAndRules create
    ;;
  "stop")
    SetsAndRules flush
    Table delete
    ;;
  "ban"|"unban")
    SetIPTypeAndExecute
    ;;
  "add"|"del")
    AdviseReaction
    ;;
  "services")
    for i in $(seq 0 $((${#mysetsAlert[@]}-1))); do
      tmpvar=$(echo $myservices|grep ${mysetsAlert[$i]})

      if [ "x$tmpvar" != "x" -o "x${mysetsAlert[0]}" = "ALL" ]; then
        services "$(echo $myservices|cut -d " " -f4,5,6,7|sed 's/{//g')"
      fi
    done
    ;;
  *)
    ;;
esac

exit 0
