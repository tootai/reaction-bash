local banFor(time,stream) =
  local dash_stream = if stream == '' then '' else '-' + stream;
  {
    ban: {
      //cmd: ['nft46'|'iptables46', 'add element inet reaction banX%s { <ip> }' % dash_stream],
      cmd: ['reaction.sh', 'ban', 'add element inet reaction banX%s { <ip> }' % dash_stream],
    },
    unban: {
      //cmd: ['nft46'|'iptables46', 'delete element inet reaction banX%s { <ip> }' % dash_stream],
      cmd: ['reaction.sh', 'unban', 'delete element inet reaction banX%s { <ip> }' % dash_stream],
      after: time,
    },
};

{
  patterns: {
    ip: {
      regex: @'(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9]))',
      ignore: [
        '127.0.0.1',    // do not ban localhost!
        '::1',          // do not ban localhost!

        // it can be also advised to avoid banning your Internet Gateway (the router)
        //
        ´192.168.0.254',
        '192.168.0.1',
      ],
      ignoreregex: [
        @'192\.168\.[0-9]{1,3}\.[0-9]{1,3}',
        @'10\.[0-9]{1,3}\[0-9]{1,3}\.[0-9]{1,3}',
        @'172\.(1[6-9]|2[0-9]|3[01])\.[0-9]{1,3}\.[0-9]{1,3}',
        @'::ffff:192\.168\.[0-9]{1,3}\.[0-9]{1,3}',
        @'::ffff:10\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}',
        @'::ffff:172\.(1[6-9]|2[0-9]|3[01])\.[0-9]{1,3}\.[0-9]{1,3}',
      ],
    },
  },

  start: [
    ['reaction.sh', 'start']
  ],

  stop: [
    ['reaction.sh', 'stop']
  ],

  streams: {
    // Ban hosts failing to connect via ssh
    //
    ssh: {
      cmd: ['/usr/bin/journalctl', '-fn0', '-u', 'sshd.service'],
      filters: {
        failedlogin: {
          regex: [
            @'authentication failure;.*rhost=<ip>',
            @'Connection reset by authenticating user .* <ip>',
            @'Failed password for .* from <ip>',
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h',''),
        },
      },
    },

    // Ban hosts failing to connect via smtp
    //
    smtp: {
      cmd: ['/usr/bin/journalctl', '-fn0', '-u', 'postfix*'],
      filters: {
        smtpban: {
          regex: [
            @'warning: unknown\[<ip>\]: SASL .* authentication failed: .*',
            @'warning: non-SMTP command from unknown\[<ip>\]: .*',
            @'warning: hostname .* does not resolve to address <ip>.*$',
            @'SSL_accept error from unknown\[<ip>\]: Connection reset by peer$',
            @'improper command pipelining after CONNECT from unknown\[<ip>\].*',
          ],
          retry: 2,
          retryperiod: '30m',
          actions: banFor('48h','smtp'),
        },
      },
    },

    // In /etc/asterisk/logger.conf set
    // messages => notice,warning,error
    //
    asterisk: {                                                                     
      cmd: ['tail', '-fn0', '/var/log/asterisk/messages'],
      filters: {  
        astban: {      
          regex: [
            @"WARNING.* pjproject:.* \[<ip>\]: .*PJSIP syntax error exception when parsing 'Request Line' header on line 1 col 1.*$",
            @"WARNING.* pjproject:.*SSL SSL_ERROR_SSL.*routines::(internal error|no suitable signature algorithm|no shared cipher|bad key share).* <ip>.*$",
            @"WARNING.* pjproject:.*SSL SSL_ERROR_SSL (Handshake).*error:.*len: 0 peer: <ip>:.*$",
            @"WARNING.* pjproject:.*sip_transport.c Dropping .* bytes packet from UDP <ip>.* : PJSIP syntax error exception when parsing (?:'Contact'|'Request Line') header .*",
            @"NOTICE.* Request ('REGISTER'|'INVITE') from .* failed for '<ip>:[0-9]+' .* (Failed to authenticate|No matching endpoint found after).*$",
          ],
          retry: 1,
          retryperiod: '1h',
          actions: banFor('48h','asterisk'),
        },
      },
    },

    // Ban hosts agianst web access
    //
    ngxerr: {
      cmd: ['tail', '-f', '/var/log/nginx/access.log'],
      filters: {
        ngxban: {
          regex: [
            @'^.* forbidden.*client: <ip>, .*',
            @'<ip> .* "GET /(wp|wordpress|wp-login.php|wp-admin/.*|blog|new|old|newsite|oldsite|test|testing|core|home) .*',
          ],
          retry: 2,
          retryperiod: '1h',
          actions: banFor('48h','web'),
        },
      },
    },

    // Manually ban IP using reaction.sh add <ip>
    // don't forget to create /var/log/reaction directory
    //
    manual: {
      cmd: ['tail', '-fn0', '/var/log/reaction/toban'],
      filters: {
        quick: {
          regex: [
            @'^<ip>.*',
          ],
          retry: 1,
          retryperiod: '1s',
          actions: banFor('48h',''),
        },
      },
    },
  },
}
